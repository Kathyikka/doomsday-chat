package com.st2projects;

import com.st2projects.cli.CLIService;
import com.st2projects.config.SpringConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Service;
import picocli.CommandLine;

@Service
public class Launcher
{
    private CLIService cliService;

    public static void main( String[] args )
    {
        AnnotationConfigApplicationContext configApplicationContext = new AnnotationConfigApplicationContext( SpringConfig.class);
        Launcher launcher = configApplicationContext.getBean( Launcher.class );
        launcher.launch(args);
    }

    void launch( String[] args )
    {
        new CommandLine( cliService ).execute( args );
    }

    @Autowired
    public void setRepl( CLIService cliService )
    {
        this.cliService = cliService;
    }
}
