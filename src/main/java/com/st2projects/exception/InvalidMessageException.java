package com.st2projects.exception;

public class InvalidMessageException extends Exception
{
    public InvalidMessageException( String format, Object... parts )
    {
        super( String.format( format, parts ) );
    }
}
