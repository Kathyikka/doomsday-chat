package com.st2projects.exception;

public class ServerException extends Exception
{
    private static final String DEFAULT_MESSAGE = "No available ports found in range %s to %s";

    public ServerException( String message, Object... params )
    {
        super( String.format( message, params ) );
    }

    public ServerException( int from, int to ) throws ServerException
    {
        throw new ServerException( DEFAULT_MESSAGE, from, to );
    }
}
