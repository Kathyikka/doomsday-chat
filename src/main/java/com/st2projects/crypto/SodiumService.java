package com.st2projects.crypto;

import com.goterl.lazysodium.LazySodiumJava;
import com.goterl.lazysodium.SodiumJava;
import com.goterl.lazysodium.exceptions.SodiumException;
import com.goterl.lazysodium.interfaces.SecretBox;
import com.goterl.lazysodium.utils.Key;
import com.goterl.lazysodium.utils.KeyPair;
import com.goterl.lazysodium.utils.SessionPair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.nio.charset.StandardCharsets;

@Service
public class SodiumService
{
    private static final SodiumJava SODIUM = new SodiumJava();
    private static final LazySodiumJava LAZY_SODIUM = new LazySodiumJava( SODIUM, StandardCharsets.UTF_8 );

    private static final Logger logger = LoggerFactory.getLogger( SodiumService.class );

    public LazySodiumJava getSodium()
    {
        return LAZY_SODIUM;
    }

    public KeyPair getNewKxKeypair()
    {
        return LAZY_SODIUM.cryptoKxKeypair();
    }

    public SessionPair getServerSessionPair( Key clientKey, KeyPair kxPair ) throws SodiumException
    {
        return LAZY_SODIUM.cryptoKxServerSessionKeys( kxPair.getPublicKey(), kxPair.getSecretKey(), clientKey );
    }

    public SessionPair getClientSessionPair( Key serverKey, KeyPair kxPair ) throws SodiumException
    {
        return LAZY_SODIUM.cryptoKxClientSessionKeys( kxPair.getPublicKey(), kxPair.getSecretKey(), serverKey );
    }

    public String encrypt( String plaintext, byte[] key ) throws SodiumException
    {
        byte[] nonce = LAZY_SODIUM.nonce( SecretBox.NONCEBYTES );
        String enc = LAZY_SODIUM.cryptoSecretBoxEasy( plaintext, nonce, Key.fromBytes( key ) );

        String finalCipher = String.join( ".", enc, LAZY_SODIUM.sodiumBin2Hex( nonce ) );

        logger.debug( "Returning string [{}]", finalCipher );

        return finalCipher;
    }

    public String decrypt( String ciphertext, byte[] key ) throws SodiumException
    {
        logger.debug( "Decrypting [{}]", ciphertext );

        String[] messageParts = ciphertext.split( "\\." );
        String cipher = messageParts[ 0 ];
        byte[] nonce = LAZY_SODIUM.sodiumHex2Bin( messageParts[ 1 ] );
        return LAZY_SODIUM.cryptoSecretBoxOpenEasy( cipher, nonce, Key.fromBytes( key ) );
    }
}
