package com.st2projects.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.st2projects")
public class SpringConfig
{
}
