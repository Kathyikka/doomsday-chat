package com.st2projects.cli;

import com.st2projects.server.ChatServer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import picocli.CommandLine;

@CommandLine.Command( name = "chat", mixinStandardHelpOptions = true )
@Service
public class CLIService implements Runnable
{
    private REPL repl;
    private ChatServer chatServer;

    @CommandLine.Option( names = { "-r" }, description = "Run in REPL mode, normally as an end user client", defaultValue = "false" )
    private boolean runRepl;

    public void run()
    {
        if( runRepl )
        {
            repl.run();
        }
        else
        {
            chatServer.run();
        }
    }

    @Autowired
    public void setRepl( REPL repl )
    {
        this.repl = repl;
    }

    @Autowired
    public void setServer( ChatServer chatServer )
    {
        this.chatServer = chatServer;
    }
}
