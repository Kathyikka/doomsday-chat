package com.st2projects.cli;

import com.st2projects.runners.Runner;
import com.st2projects.server.ChatServer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.Scanner;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

@Service
public class REPL
{
    private List<Runner> runners;
    private ChatServer chatServer;

    public void run()
    {
        boolean run = true;

        Executor executor = Executors.newSingleThreadExecutor();
        executor.execute( chatServer );

        while( run )
        {
            System.out.print( "\n\n>>> " );
            Scanner scanner = new Scanner( System.in );

            String replCommandString = scanner.nextLine();

            String[] commandStringParts = replCommandString.split( "\s+" );

            String command = commandStringParts[ 0 ];
            String[] args = Arrays.copyOfRange( commandStringParts, 1, commandStringParts.length );

            Optional<Runner> commandRunnerOptional = findRunnerForName( command );

            commandRunnerOptional.ifPresentOrElse( r -> r.run( args ), () -> System.out.printf( "Unknown command [%s] - use help to see all commandlets %n", command ) );
        }
    }

    private Optional<Runner> findRunnerForName( String name )
    {
        return runners.stream().filter( r -> r.getName().equals( name ) ).findFirst();
    }

    @Autowired
    public void setRunners( List<Runner> runners )
    {
        this.runners = runners;
    }

    @Autowired
    public void setChatServer( ChatServer chatServer )
    {
        this.chatServer = chatServer;
    }
}
