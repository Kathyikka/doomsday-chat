package com.st2projects.server;

import com.st2projects.exception.ServerException;
import org.springframework.stereotype.Service;

@Service
public class MessageDecoder
{
    public MessageData decode( String message ) throws ServerException
    {
        String[] parts = message.trim().split( "\s+" );

        String port = null;
        String msg = null;

        for( String part : parts )
        {
            String[] keyValuePairs = part.split( "=" );

            switch( keyValuePairs[ 0 ] )
            {
                case "port" -> port = keyValuePairs[ 1 ];
                case "msg" -> msg = keyValuePairs[ 1 ];
                default -> throw new ServerException( "Invalid message part [%s]", keyValuePairs[ 0 ] );
            }
        }

        if( port == null || msg == null )
        {
            throw new ServerException( "Incomplete message [%s]", message );
        }

        return new MessageData( port, msg );
    }
}
