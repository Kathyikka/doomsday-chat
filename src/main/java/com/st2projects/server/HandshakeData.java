package com.st2projects.server;

import com.goterl.lazysodium.utils.Key;

public record HandshakeData( String name, Key clientKey, String port, String testString )
{
    public int portAsInt()
    {
        return Integer.parseInt( port );
    }
}
