package com.st2projects.server;

import com.st2projects.exception.InvalidMessageException;

import java.nio.charset.StandardCharsets;
import java.util.UUID;

public record Message( RequestType requestType, UUID nodeID, String content )
{
    private static final String DELIMITER = "\\|";

    public Message( RequestType requestType, String uuid, int port )
    {
        this( requestType, UUID.fromString( uuid ), Integer.toString( port ) );
    }

    static Message parseMessage( String messageString ) throws InvalidMessageException
    {
        String[] spiltMessage = messageString.trim().split( DELIMITER );

        RequestType requestType;
        UUID nodeID;
        String content;

        if( spiltMessage.length == 3 )
        {
            requestType = RequestType.getByName( spiltMessage[ 0 ] ).orElseThrow( () -> new InvalidMessageException( "Unknown message type [%s]", spiltMessage[ 0 ] ) );
            nodeID = UUID.fromString( spiltMessage[ 1 ] );
            content = spiltMessage[ 2 ];
        }
        else
        {
            throw new InvalidMessageException( "Message too long, expected 3 parts but found [%s] - [%s]", spiltMessage.length, messageString );
        }

        return new Message( requestType, nodeID, content );
    }

    @Override
    public String toString()
    {
        return String.format( "%s|%s|%s", requestType, nodeID, content );
    }

    public byte[] getBytes()
    {
        return toString().getBytes( StandardCharsets.UTF_8 );
    }
}
