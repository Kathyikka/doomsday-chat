package com.st2projects.server;

import com.goterl.lazysodium.exceptions.SodiumException;
import com.goterl.lazysodium.utils.KeyPair;
import com.goterl.lazysodium.utils.SessionPair;
import com.st2projects.crypto.SodiumService;
import com.st2projects.exception.InvalidMessageException;
import com.st2projects.exception.ServerException;
import com.st2projects.runners.ExitRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.Optional;

@Service
public class ChatServer implements Runnable
{
    private static final Logger logger = LoggerFactory.getLogger( ChatServer.class );

    private ConnectionRepo connectionRepo;
    private ServerData serverData;
    private HandshakeDecoder handshakeDecoder;
    private SodiumService sodiumService;
    private KeyPortPairRepo keyPortPairRepo;
    private MessageDecoder messageDecoder;

    public void run()
    {
        Message message = null;

        try( DatagramSocket socket = new DatagramSocket( serverData.getPort(), InetAddress.getByName( "0.0.0.0" ) ) )
        {
            socket.setBroadcast( true );

            while( true )
            {
                logger.info( "Waiting for requests on port [{}]", serverData.getPort() );

                byte[] recvBuf = new byte[ 15000 ];
                DatagramPacket packet = new DatagramPacket( recvBuf, recvBuf.length );
                try
                {
                    socket.receive( packet );
                }
                catch( IOException e )
                {
                    e.printStackTrace();
                }

                String data = new String( packet.getData() ).trim();
                logger.info( "Received [{}]", data );

                message = Message.parseMessage( data );

                switch( message.requestType() )
                {
                    case HANDSHAKE_REQ -> handleHandshakeRequest( message, socket, packet.getAddress() );
                    case HANDSHAKE_RESP -> handleHandshakeResponse( message, socket, packet.getAddress() );
                    case HANDSHAKE_FINAL -> handleHandshakeFinal( message );
                    case MESSAGE -> handleMessage( message );
                }
            }

        }
        catch( IOException e )
        {
            try
            {
                serverData.notifyPortInUse();
            }
            catch( ServerException ex )
            {
                logger.error( "Failed to find free port - stopping", ex );
                new ExitRunner().run();
            }
            run();
        }
        catch( InvalidMessageException e )
        {
            logger.error( "Failed to handle message [{}]", message, e );
        }
        catch( ServerException | SodiumException e )
        {
            logger.error( "Failed handshake", e );
        }
    }

    void handleHandshakeRequest( Message message, DatagramSocket socket, InetAddress returnAddress ) throws ServerException, SodiumException, IOException
    {
        HandshakeData handshakeData = handshakeDecoder.decodeHandshakeData( message.content() );

        KeyPair kxKeypair = sodiumService.getNewKxKeypair();
        SessionPair serverSessionPair = sodiumService.getServerSessionPair( handshakeData.clientKey(), kxKeypair );
        SessionPair clientSessionPair = sodiumService.getClientSessionPair( handshakeData.clientKey(), kxKeypair );

        Message handshakeResponse = new Message( RequestType.HANDSHAKE_RESP, serverData.getNodeId(), String.format( "name=%s key=%s port=%s test=%s",
                serverData.getNodeId(), kxKeypair.getPublicKey().getAsHexString(), serverData.getPort(), sodiumService.encrypt( RequestType.HANDSHAKE_RESP.protocolContent, serverSessionPair.getTx() ) ) );
        byte[] responseBytes = handshakeResponse.getBytes();

        DatagramPacket responsePacket = new DatagramPacket( responseBytes, responseBytes.length, returnAddress, handshakeData.portAsInt() );
        socket.send( responsePacket );

        connectionRepo.addConnection( new Connection( handshakeData.name(), handshakeData.port(), serverSessionPair, clientSessionPair ) );
    }

    void handleHandshakeResponse( Message message, DatagramSocket socket, InetAddress returnAddress ) throws ServerException, SodiumException, IOException
    {
        HandshakeData handshakeData = handshakeDecoder.decodeHandshakeData( message.content() );

        KeyPair kxKeypair = keyPortPairRepo.getKeypairForPort( Integer.parseInt( handshakeData.port() ) );

        SessionPair serverSessionPair = sodiumService.getServerSessionPair( handshakeData.clientKey(), kxKeypair );
        SessionPair clientSessionPair = sodiumService.getClientSessionPair( handshakeData.clientKey(), kxKeypair );
        testHandshake( handshakeData.testString(), clientSessionPair, RequestType.HANDSHAKE_RESP );

        connectionRepo.addConnection( new Connection( handshakeData.name(), handshakeData.port(), serverSessionPair, clientSessionPair ) );

        Message handshakeFinal = new Message( RequestType.HANDSHAKE_FINAL, serverData.getNodeId(), String.format( "port=%s msg=%s",
                serverData.getPort(), sodiumService.encrypt( RequestType.HANDSHAKE_FINAL.protocolContent, serverSessionPair.getTx() ) ) );

        byte[] handshakeFinalBytes = handshakeFinal.getBytes();

        DatagramPacket datagramPacket = new DatagramPacket( handshakeFinalBytes, handshakeFinalBytes.length, returnAddress, handshakeData.portAsInt() );

        socket.send( datagramPacket );
    }

    void handleHandshakeFinal( Message message ) throws ServerException, SodiumException
    {
        MessageData messageData = messageDecoder.decode( message.content() );
        Optional<Connection> connectionOptional = connectionRepo.getConnectionByPortAndID( messageData.getPortAsInt(), message.nodeID().toString() );

        if( connectionOptional.isPresent() )
        {
            testHandshake( messageData.msg(), connectionOptional.get().clientSessionPair(), RequestType.HANDSHAKE_FINAL );
        }
        else
        {
            throw new ServerException( "Invalid port / id pair %s %s", messageData.port(), message.nodeID() );
        }
    }

    void handleMessage( Message message ) throws ServerException, SodiumException
    {
        MessageData messageData = messageDecoder.decode( message.content() );

        Optional<Connection> connectionOptional = connectionRepo.getConnectionByPortAndID( messageData.getPortAsInt(), message.nodeID().toString() );

        if( connectionOptional.isPresent() )
        {
            Connection connection = connectionOptional.get();
            SessionPair sessionPair = connection.clientSessionPair();
            String decryptedPayload = sodiumService.decrypt( messageData.msg(), sessionPair.getRx() );
            logger.info( "New message from [{}] - [{}]", message.nodeID(), decryptedPayload );
        }
        else
        {
            throw new ServerException( "Invalid port / id pair %s %s", messageData.port(), message.nodeID() );
        }
    }

    void testHandshake( String handshakeTestString, SessionPair sessionPair, RequestType requestType ) throws SodiumException, ServerException
    {
        String decrypted = sodiumService.decrypt( handshakeTestString, sessionPair.getRx() );

        if( !decrypted.equals( requestType.protocolContent ) )
        {
            throw new ServerException( "Handshake failed, test string incorrect" );
        }
        else
        {
            logger.info( "Successful handshake - content[{}]", decrypted );
        }
    }

    @Autowired
    public void setConnectionRepo( ConnectionRepo connectionRepo )
    {
        this.connectionRepo = connectionRepo;
    }

    @Autowired
    public void setServerData( ServerData serverData )
    {
        this.serverData = serverData;
    }

    @Autowired
    public void setHandshakeDecoder( HandshakeDecoder handshakeDecoder )
    {
        this.handshakeDecoder = handshakeDecoder;
    }

    @Autowired
    public void setSodiumService( SodiumService sodiumService )
    {
        this.sodiumService = sodiumService;
    }

    @Autowired
    public void setKeyPortPairRepo( KeyPortPairRepo keyPortPairRepo )
    {
        this.keyPortPairRepo = keyPortPairRepo;
    }

    @Autowired
    public void setMessageDecoder( MessageDecoder messageDecoder )
    {
        this.messageDecoder = messageDecoder;
    }
}
