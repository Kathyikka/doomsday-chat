package com.st2projects.server;

public record MessageData( String port, String msg )
{
    public int getPortAsInt()
    {
        return Integer.parseInt( port );
    }
}
