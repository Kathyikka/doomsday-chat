package com.st2projects.server;

import com.st2projects.exception.ServerException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public class ServerData implements InitializingBean
{
    private static final Logger logger = LoggerFactory.getLogger( ServerData.class );
    private static final UUID NODE_ID = UUID.randomUUID();

    private static final int MIN_PORT = 8000;
    private static final int MAX_PORT = 9000;

    // Start at port 8000, if this port is in use we find another
    private int port = MIN_PORT;

    public UUID getNodeId()
    {
        return NODE_ID;
    }

    public int getPort()
    {
        return port;
    }

    public int getMinPort()
    {
        return MIN_PORT;
    }

    public int getMaxPort()
    {
        return MAX_PORT;
    }

    public void notifyPortInUse() throws ServerException
    {
        if( port < MAX_PORT )
        {
            port++;
        }
        else
        {
            throw new ServerException( MIN_PORT, MAX_PORT );
        }
    }

    @Override
    public void afterPropertiesSet() throws Exception
    {
        logger.info( "Server id [{}]", NODE_ID );
    }
}
