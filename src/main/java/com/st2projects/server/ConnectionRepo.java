package com.st2projects.server;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Repository
public class ConnectionRepo
{
    private static final Logger logger = LoggerFactory.getLogger( ConnectionRepo.class );
    List<Connection> connections = new ArrayList<>();

    public List<Connection> getConnections()
    {
        return connections;
    }

    public void addConnection( Connection connection )
    {
        logger.info( "Adding {}", connection );
        connections.add( connection );
    }

    public Optional<Connection> getConnectionByPortAndID( int port, String Id )
    {
        return connections.stream().filter( c -> c.portAsInt() == ( port ) && c.name().equals( Id ) ).findFirst();
    }
}
