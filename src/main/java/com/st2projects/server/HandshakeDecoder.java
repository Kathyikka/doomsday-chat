package com.st2projects.server;

import com.goterl.lazysodium.utils.Key;
import com.st2projects.exception.ServerException;
import org.springframework.stereotype.Service;

@Service
public class HandshakeDecoder
{
    public HandshakeData decodeHandshakeData( String data ) throws ServerException
    {
        String[] splitData = data.split( "\s+" );

        String name = null;
        String key = null;
        String port = null;
        String test = null;

        for( String components : splitData )
        {
            String[] keyValuePairs = components.split( "=" );

            switch( keyValuePairs[ 0 ] )
            {
                case "name" -> name = keyValuePairs[ 1 ];
                case "key" -> key = keyValuePairs[ 1 ];
                case "port" -> port = keyValuePairs[ 1 ];
                case "test" -> test = keyValuePairs[ 1 ];
                default -> throw new ServerException( "Unknown handshake component - [%s]", keyValuePairs[ 0 ] );
            }
        }

        if( name == null || key == null )
        {
            throw new ServerException( "Incomplete handshake name[%s], key[%s], port[%s]", name, key, port );
        }

        return new HandshakeData( name, Key.fromHexString( key ), port, test );
    }
}
