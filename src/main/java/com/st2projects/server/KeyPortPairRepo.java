package com.st2projects.server;

import com.goterl.lazysodium.utils.KeyPair;
import com.st2projects.exception.ServerException;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.Map;

@Repository
public class KeyPortPairRepo
{
    private static final Map<Integer, KeyPair> KEY_PORT_PAIRING = new HashMap<>();

    public KeyPair getKeypairForPort( int port ) throws ServerException
    {
        KeyPair keyPair;
        if( KEY_PORT_PAIRING.containsKey( port ) )
        {
            keyPair = KEY_PORT_PAIRING.get( port );
        }
        else
        {
            throw new ServerException( "Cannot find keypair for port [%s]", port );
        }

        return keyPair;
    }

    public void add( int port, KeyPair keyPair )
    {
        KEY_PORT_PAIRING.put( port, keyPair );
    }
}
