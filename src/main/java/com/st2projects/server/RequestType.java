package com.st2projects.server;

import java.util.Arrays;
import java.util.Optional;

public enum RequestType
{
    HANDSHAKE_REQ( null ),
    HANDSHAKE_RESP( "Hello_new_friend" ),
    HANDSHAKE_FINAL( "Good_to_see_you_old_chap" ),
    DISCOVER( null ),
    DISCOVER_RESPONSE( null ),
    MESSAGE( null );

    final String protocolContent;

    RequestType( String protocolContent )
    {
        this.protocolContent = protocolContent;
    }

    public static Optional<RequestType> getByName( String name )
    {
        return Arrays.stream( values() ).filter( r -> r.name().equalsIgnoreCase( name ) ).findFirst();
    }
}
