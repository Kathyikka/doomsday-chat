package com.st2projects.server;

import com.goterl.lazysodium.utils.SessionPair;

public record Connection( String name, String port, SessionPair serverSessionPair, SessionPair clientSessionPair )
{
    public int portAsInt()
    {
        return Integer.parseInt( port );
    }
}
