package com.st2projects.runners;

import com.goterl.lazysodium.utils.KeyPair;
import com.st2projects.crypto.SodiumService;
import com.st2projects.server.KeyPortPairRepo;
import com.st2projects.server.Message;
import com.st2projects.server.RequestType;
import com.st2projects.server.ServerData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.stream.IntStream;

@Service
public class ConnectionRunner implements Runner
{
    private static final Logger logger = LoggerFactory.getLogger( ConnectionRunner.class );

    private ServerData serverData;
    private SodiumService sodiumService;
    private KeyPortPairRepo keyPortPairRepo;

    @Override
    public String getName()
    {
        return "connect";
    }

    @Override
    public void run( String... args )
    {
        try
        {
            DatagramSocket socket = new DatagramSocket();
            socket.setBroadcast( true );

            if( args.length >= 1 )
            {
                int port = Integer.parseInt( args[ 0 ] );
                if( port != serverData.getPort() )
                {
                    sendDiscoverRequest( Integer.parseInt( args[ 0 ] ), socket );
                }
                else
                {
                    logger.warn( "Will not connect to myself - listening on port [{}]", port );
                }
            }
            else
            {
                // Send connection requests to all ports except our own
                IntStream.rangeClosed( serverData.getMinPort(), serverData.getMaxPort() ).filter( port -> port != serverData.getPort() ).forEach( port -> sendDiscoverRequest( port, socket ) );
            }

        }
        catch( IOException e )
        {
            e.printStackTrace();
        }
    }

    void sendDiscoverRequest( int port, DatagramSocket socket )
    {
        try
        {
            KeyPair kxKeypair = sodiumService.getNewKxKeypair();

            keyPortPairRepo.add( port, kxKeypair );
            byte[] data = new Message( RequestType.HANDSHAKE_REQ, serverData.getNodeId(), String.format( "name=%s key=%s port=%s", serverData.getNodeId(), kxKeypair.getPublicKey().getAsHexString(), serverData.getPort() ) ).getBytes();

            DatagramPacket packet = new DatagramPacket( data, data.length, InetAddress.getByName( "255.255.255.255" ), port );
            socket.send( packet );
        }
        catch( IOException io )
        {
            logger.error( "Failed to send request on port [{}]", port, io );
        }
    }

    @Autowired
    public void setServerData( ServerData serverData )
    {
        this.serverData = serverData;
    }

    @Autowired
    public void setSodiumService( SodiumService sodiumService )
    {
        this.sodiumService = sodiumService;
    }

    @Autowired
    public void setKeyPortPairRepo( KeyPortPairRepo keyPortPairRepo )
    {
        this.keyPortPairRepo = keyPortPairRepo;
    }
}
