package com.st2projects.runners;

public interface Runner
{
    String getName();

    void run( String... args );
}
