package com.st2projects.runners;

import org.springframework.stereotype.Service;

@Service
public class HelpRunner implements Runner
{
    @Override
    public String getName()
    {
        return "help";
    }

    @Override
    public void run( String... args )
    {
        System.out.println( "You're on your own for now" );
    }
}
