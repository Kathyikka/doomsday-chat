package com.st2projects.runners;

import org.springframework.stereotype.Component;

@Component
public class PrintRunner implements Runner
{
    @Override
    public String getName()
    {
        return "print";
    }

    @Override
    public void run( String... args )
    {
        String toPrint = String.join( " ", args );

        System.out.println( toPrint );
    }
}
