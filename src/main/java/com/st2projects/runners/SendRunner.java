package com.st2projects.runners;

import com.goterl.lazysodium.exceptions.SodiumException;
import com.goterl.lazysodium.utils.SessionPair;
import com.st2projects.crypto.SodiumService;
import com.st2projects.server.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

@Service
public class SendRunner implements Runner
{
    private static final Logger logger = LoggerFactory.getLogger( SendRunner.class );

    private ConnectionRepo connectionRepo;
    private ServerData serverData;
    private SodiumService sodiumService;

    @Override
    public String getName()
    {
        return "send";
    }

    @Override
    public void run( String... args )
    {
        try
        {
            DatagramSocket socket = new DatagramSocket();
            socket.setBroadcast( true );


            for( Connection connection : connectionRepo.getConnections() )
            {
                SessionPair sessionPair = connection.serverSessionPair();
                String encryptedPayload = sodiumService.encrypt( String.join( " ", args ), sessionPair.getTx() );

                byte[] messageBytes = new Message( RequestType.MESSAGE, serverData.getNodeId(), String.format( "port=%s msg=%s", serverData.getPort(), encryptedPayload ) ).getBytes();

                DatagramPacket packet = new DatagramPacket( messageBytes, messageBytes.length, InetAddress.getByName( "255.255.255.255" ), Integer.parseInt( connection.port() ) );
                socket.send( packet );
            }
        }
        catch( IOException | SodiumException e )
        {
            logger.error( "Failed to send message", e );
        }
    }

    @Autowired
    public void setConnectionRepo( ConnectionRepo connectionRepo )
    {
        this.connectionRepo = connectionRepo;
    }

    @Autowired
    public void setServerData( ServerData serverData )
    {
        this.serverData = serverData;
    }

    @Autowired
    public void setSodiumService( SodiumService sodiumService )
    {
        this.sodiumService = sodiumService;
    }
}
