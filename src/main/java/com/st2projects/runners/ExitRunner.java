package com.st2projects.runners;

import org.springframework.stereotype.Service;

@Service
public class ExitRunner implements Runner
{
    @Override
    public String getName()
    {
        return "exit";
    }

    @Override
    public void run( String... args )
    {
        System.exit( 0 );
    }
}
