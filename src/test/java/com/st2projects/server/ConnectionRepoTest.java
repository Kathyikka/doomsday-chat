package com.st2projects.server;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatCode;

class ConnectionRepoTest
{
    @Test
    public void testAddNewConnectionDoesNotThrow()
    {
        // given
        ConnectionRepo connectionRepo = new ConnectionRepo();

        // then
        assertThatCode(() -> connectionRepo.addConnection( new Connection( null, null, null, null ) )).doesNotThrowAnyException();
    }

    @Test
    public void testGetConnectionByPortAndIDReturnsCorrectConnection()
    {
        // given
        Connection connection1 = new Connection( "con1", "1", null, null );
        Connection connection2 = new Connection( "con2", "2", null, null );
        ConnectionRepo connectionRepo = new ConnectionRepo();
        connectionRepo.addConnection( connection1 );
        connectionRepo.addConnection( connection2 );

        // then
        assertThat( connectionRepo.getConnectionByPortAndID( 1, "con1" ).orElse( null ) ).isNotNull().isEqualTo( connection1 );
    }

    @Test
    public void testGetConnectionByPortAndIDReturnsEmptyOptionalWhenNotFound()
    {
        // given
        Connection connection1 = new Connection( "con1", "1", null, null );
        ConnectionRepo connectionRepo = new ConnectionRepo();
        connectionRepo.addConnection( connection1 );


        // then
        assertThat( connectionRepo.getConnectionByPortAndID( 8888, "abc" ) ).isEmpty();
    }
}
