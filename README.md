# doomsday-chat

A toy peer to peer chat server with auto-discover features and e2e encryption

## Installation

Installation is simple, simply clone the repo and compile with maven:

```shell
mvn clean install
```

A jar will be produced in the `target/` folder
